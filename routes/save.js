var express = require('express');
var router = express.Router();

router.post('/save', function(req, res) {
    var db = req.db;

    var list = req.body.list;

    console.info("trying to drop collection items");
    db.collection('items').drop(function() {
        console.info("collection items dropped");
        db.collection('items').insert(JSON.parse(list), function() {
            res.status(200).end();
        });
    });
});

module.exports = router;
