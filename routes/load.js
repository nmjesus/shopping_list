var express = require('express');
var router = express.Router();

router.get('/load', function(req, res) {
    var db = req.db;

    db.collection('items').find().toArray(function(err, items) {
        res.set('Content-type', 'application/json');
        res.status(200).end(JSON.stringify(items));
    });
});

module.exports = router;
