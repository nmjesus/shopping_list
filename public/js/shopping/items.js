/*globals ko, Ink, alert*/
(function() {
    'use strict';

    var AUTO_INCREMENT = 0;
    var Item = function(id, name, inBag) {
        this.id = id;
        this.inBag = ko.observable(inBag);
        this.name = ko.observable(name);
    };


    var ShoppingListViewModel = function() {
        this.items = ko.observableArray([]);

        this.submit = function(form) {
            var itemElm = form.elements[0],
                name = itemElm.value;

            itemElm.value = '';
            return this.addItem(name, false, true);
        };

        var exists = function(name) {
            var items = this.items();
            var i = 0, len = items.length;
            for(i = 0; i<len; i++) {
                if(items[i].name() === name) {
                    return true;
                }
            }
            return false;
        }.bind(this);


        this.addItem = function(name, inBag, save) {
            var item = new Item(++AUTO_INCREMENT, name, inBag);
            if(!exists(name)) {
                this.items.push(item);
                if(save) {
                    this.save();
                }
                return item;
            }
            return false;
        };

        this.save = function() {
            var params = 'list=' + ko.toJSON(this.items);
            return new Ink.Net.Ajax('/save', {
                parameters: params,
                onSuccess: function(req, res) {
                    //alert("Data saved");
                },
                onFailure: function(req, res) {
                    alert("Error occurred during saving data");
                }
            });
        }.bind(this);
    };

    var _sl = new ShoppingListViewModel();
    ko.applyBindings(_sl, Ink.s("#shopping_view"));

    return new Ink.Net.Ajax('/load', {
        method: 'get',
        onSuccess: function(req, res) {
            var items = res;

            var i = 0, len = items.length;
            for(i = 0; i<len; i++) {
                _sl.addItem(items[i].name, items[i].inBag, false);
            }
        }
    });
}());
